__author__ = "Jonathan Heno"
__credits__ = ["Jonathan Heno"]
__version__ = "2.0.0"
__maintainer__ = "Jonathan Heno"
__email__ = ["jonathan.heno@alumni.univ-avignon.fr", "jheno84@gmail.com"]
__status__ = "dev"


import plotly
import plotly.colors
import plotly.express
import plotly.subplots
import plotly.graph_objects
import numpy
import pandas
import pathlib
import typing

BASE_LAYOUT: dict = {
    "title": {
        "text": "",
        "font_size": 16,
        "xref": "container",
        "xanchor": "center",
        "yanchor": "top",
        "y": 0.975,
        "x": 0.5,
    },
    "font": {
        "family": "Overpass, Raleway, Open Sans, Droid Sans Mono, Courier New, monospace",
        "size": 12,
        "color": "#c5c8c6",
    },
    "margin": {"l": 0, "r": 0, "b": 0, "t": 100.0, "autoexpand": True, "pad": 0},
    "autosize": True,
    "paper_bgcolor": "#282a2e",
    "plot_bgcolor": "#373b41",
}


def plot_ssp_dist(df: pandas.DataFrame, cfg: dict, dir: pathlib.Path):
    probabilities: typing.List[numpy.float32] = df.sort_values(
        by="p", ascending=True
    ).p.unique()

    plt_opts: typing.Callable = lambda p, pi: {
        "mode": "markers",
        "marker": {
            "size": 3,
            "color": pi,
            "colorscale": "Sunset",
            "opacity": 0.6,
            "showscale": True,
            "colorbar": {
                "thickness": 7.5,
                "x": 0,
                "ticks": "inside",
                "ticklen": 7.5,
                "tickwidth": 2,
                "tickcolor": "#282a2e",
                "title": {"text": "<b>Probability scale</b>", "side": "bottom"},
            },
        },
    }

    frame_opts: typing.Callable = lambda duration: {
        "frame": {"duration": duration},
        "mode": "immediate",
        "fromcurrent": True,
        "transition": {"duration": duration, "easing": "quadratic-in-out"},
    }

    frames: typing.List[plotly.graph_objects.Frame] = []
    for p in probabilities:
        filtered_df: pandas.DataFrame = df[df.p == p]
        max_pi = filtered_df.pi.max()
        filtered_df = filtered_df[filtered_df.pi > max_pi / 2.5]
        frames += [
            plotly.graph_objects.Frame(
                data=plotly.graph_objects.Scatter3d(
                    x=filtered_df.i,
                    y=filtered_df.j,
                    z=filtered_df.k,
                    **plt_opts(p, filtered_df.pi),
                ),
                name=f"{p:.2}",
            )
        ]

    fig = plotly.graph_objects.Figure(frames=frames)

    # Add data to be displayed before animation starts
    p_0: numpy.float32 = probabilities[0]
    filtered_df: pandas.DataFrame = df[df.p == p_0]
    max_pi = filtered_df.pi.max()
    filtered_df = filtered_df[filtered_df.pi > max_pi / 3.0]
    fig.add_trace(
        plotly.graph_objects.Scatter3d(
            x=filtered_df.i,
            y=filtered_df.j,
            z=filtered_df.k,
            **plt_opts(p_0, filtered_df.pi),
        )
    )

    sliders_opts = {
        "active": 0,
        "yanchor": "top",
        "xanchor": "left",
        "currentvalue": {
            "font": {"family": "Raleway", "size": 16, "color": "#f0c674"},
            "prefix": "Probability: ",
            "visible": True,
            "xanchor": "right",
            "offset": 5,
        },
        "transition": {"duration": 250, "easing": "cubic-in-out"},
        "pad": {"b": 0, "t": 0, "r": 10, "l": 10},
        "bgcolor": "#373b41",
        "activebgcolor": "#f0c674",
        "bordercolor": "rgba(0,0,0,0)",
        "tickcolor": "#373b41",
        "tickwidth": 2,
        "len": 1.0,
        "x": 0,
        "y": 0,
        "steps": [
            {
                "args": [[frame.name], frame_opts(150)],
                "label": frame.name,
                "method": "animate",
            }
            for key, frame in enumerate(fig.frames)
        ],
    }

    of: str = f"{dir}/ssp_dist.html"

    layout: dict = BASE_LAYOUT
    layout["title"][
        "text"
    ] = f"""
        <b>Steady state distribution</b><br>
        <b>dims: {cfg['dims']}</b><br>
        <i>λ: {cfg['lambda']:.2f}, μ₁: {cfg['mu.1']:.2f}, μ: {cfg['mu']:.2f}, μ₂: {cfg['mu.2']:.2f}, R: {cfg['r']:.2f}, c₁: {cfg['c.1']:.2f}, c₂: {cfg['c.2']:.2f}</i><br>
    """
    layout["margin"]["pad"] = 10

    fig.update_layout(
        scene={
            "xaxis": {
                "range": [df.i.min(), df.i.max()],
                "autorange": False,
                "title": {
                    "text": f"<b>#S₁</b>",
                    "font": {"color": "#f0c674", "size": 16},
                },
                "color": "#c5c8c6",
                "gridcolor": "#707880",
                "tickformat": "d",
                "backgroundcolor": "#373b41",
                "ticks": "outside",
                "tickwidth": 5,
                "tickcolor": "#c5c8c6",
            },
            "yaxis": {
                "range": [df.j.min(), df.j.max()],
                "autorange": False,
                "title": {
                    "text": f"<b>#Transit</b>",
                    "font": {"color": "#f0c674", "size": 16},
                },
                "color": "#c5c8c6",
                "gridcolor": "#707880",
                "tickformat": "d",
                "backgroundcolor": "#373b41",
                "ticks": "outside",
                "tickwidth": 5,
                "tickcolor": "#c5c8c6",
            },
            "zaxis": {
                "range": [df.k.min(), df.k.max()],
                "autorange": False,
                "title": {
                    "text": f"<b>#S₂</b>",
                    "font": {"color": "#f0c674", "size": 16},
                },
                "color": "#c5c8c6",
                "gridcolor": "#707880",
                "tickformat": "d",
                "backgroundcolor": "#373b41",
                "ticks": "outside",
                "tickwidth": 5,
                "tickcolor": "#c5c8c6",
            },
            "aspectratio": {"x": 1, "y": 1, "z": 1},
        },
        updatemenus=[
            {
                "buttons": [
                    {
                        "args": [None, frame_opts(25)],
                        "label": "START",
                        "method": "animate",
                    },
                    {
                        "args": [[None], frame_opts(0)],
                        "label": "PAUSE",
                        "method": "animate",
                    },
                ],
                "direction": "right",
                "showactive": True,
                "pad": {"r": 10, "l": 10, "b": 10, "t": 10},
                "type": "buttons",
                "x": 0.5,
                "xanchor": "center",
                "y": -0.125,
                "yanchor": "top",
                "bgcolor": "#f0c674",
                "bordercolor": "rgba(0,0,0,0)",
                "font": {"size": 16, "color": "#282a2e"},
            }
        ],
        sliders=[sliders_opts],
        **layout,
    )

    fig.write_html(of)


def plot_join_ssp(df: typing.Dict[str, pandas.DataFrame], cfg: dict, dir: pathlib.Path):
    dim_indices: numpy.ndarray = numpy.sort(df.dim.unique())

    nrows: numpy.uint = len(dim_indices)

    fig = plotly.subplots.make_subplots(
        rows=nrows,
        cols=1,
        shared_xaxes=True,
        vertical_spacing=0.05,
    )

    for idx in dim_indices:
        filter_df = df[df.dim == idx].sort_values(by=["n", "p"], ascending=True)

        # ? https://plotly.com/python-api-reference/generated/plotly.express.colors.html
        color_scale: list = [
            plotly.colors.label_rgb(ctuple)
            for ctuple in plotly.express.colors.n_colors(
                plotly.colors.hex_to_rgb("#f3e69a"),
                plotly.colors.hex_to_rgb("#eb8085"),
                filter_df.n.nunique(),
            )
        ]

        for key, group in filter_df.groupby("n"):
            fig.add_trace(
                plotly.graph_objects.Scattergl(
                    x=group.p,
                    y=group.pi,
                    name=f"π[{'‧' * idx}{key}{'‧' * (len(dim_indices) - (idx + 1))}]",
                    mode="lines",
                    line={
                        "color": color_scale[key],
                        "width": 2,
                    },
                    opacity=0.75,
                ),
                row=idx + 1,
                col=1,
            )

        fig.update_yaxes(
            title={"text": f"<b>π@{idx}</b>", "font": {"color": "#f0c674", "size": 16}},
            row=idx + 1,
        )

    of: str = f"{dir}/join_ssp.html"

    layout: dict = BASE_LAYOUT
    layout["title"][
        "text"
    ] = f"""
        <b>Joined steady state probability along each dimensions</b><br>
        <i>λ: {cfg['lambda']:.2f}, μ₁: {cfg['mu.1']:.2f}, μ: {cfg['mu']:.2f}, μ₂: {cfg['mu.2']:.2f}, R: {cfg['r']:.2f}, c₁: {cfg['c.1']:.2f}, c₂: {cfg['c.2']:.2f}</i><br>
    """
    layout["margin"]["l"] = 70
    layout["margin"]["t"] = 90
    layout["margin"]["b"] = 60

    fig.update_layout(**layout)

    fig.update_xaxes(color="#c5c8c6", gridcolor="#707880")
    fig.update_yaxes(color="#c5c8c6", gridcolor="#707880")

    fig.update_xaxes(
        title={"text": "<b>p</b>", "font": {"color": "#f0c674", "size": 16}},
        row=nrows,
        col=1,
    )

    fig.write_html(of)


def plot_avg_nt(df: typing.Dict[str, pandas.DataFrame], cfg: dict, dir: pathlib.Path):
    fig = plotly.subplots.make_subplots(
        rows=1,
        cols=1,
        shared_xaxes=True,
        vertical_spacing=0.025,
    )

    sorted_df = df.sort_values(by=["dim", "p"], ascending=True)

    # ? https://plotly.com/python-api-reference/generated/plotly.express.colors.html
    color_scale: list = [
        plotly.colors.label_rgb(ctuple)
        for ctuple in plotly.express.colors.n_colors(
            plotly.colors.hex_to_rgb("#f3e69a"),
            plotly.colors.hex_to_rgb("#eb8085"),
            n_colors=sorted_df.dim.nunique(),
        )
    ]

    for key, group in sorted_df.groupby("dim"):
        fig.add_trace(
            plotly.graph_objects.Scattergl(
                x=group.p,
                y=group.avg,
                name=f"~ N@{key}",
                mode="lines",
                line={
                    "color": color_scale[key],
                    "width": 2,
                },
                opacity=0.75,
            ),
            row=1,
            col=1,
        )

    of: str = f"{dir}/avg_nt.html"

    layout: dict = BASE_LAYOUT
    layout["title"][
        "text"
    ] = f"""
        <b>Average number of tasks on each server</b><br>
        <i>λ: {cfg['lambda']:.2f}, μ₁: {cfg['mu.1']:.2f}, μ: {cfg['mu']:.2f}, μ₂: {cfg['mu.2']:.2f}, R: {cfg['r']:.2f}, c₁: {cfg['c.1']:.2f}, c₂: {cfg['c.2']:.2f}</i><br>
    """
    layout["margin"]["l"] = 70
    layout["margin"]["t"] = 90
    layout["margin"]["b"] = 60

    fig.update_layout(
        xaxis={
            "title": {"text": f"<b>p</b>", "font": {"color": "#f0c674", "size": 16}},
            "color": "#c5c8c6",
            "gridcolor": "#707880",
        },
        yaxis={
            "title": {
                "text": f"<b>Avg. #tasks</b>",
                "font": {"color": "#f0c674", "size": 16},
            },
            "color": "#c5c8c6",
            "gridcolor": "#707880",
        },
        **layout,
    )

    fig.write_html(of)
