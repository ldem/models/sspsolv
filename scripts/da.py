__author__ = "Jonathan Heno"
__credits__ = ["Jonathan Heno"]
__version__ = "2.0.0"
__maintainer__ = "Jonathan Heno"
__email__ = ["jonathan.heno@alumni.univ-avignon.fr", "jheno84@gmail.com"]
__status__ = "dev"

import pandas
import numpy
import pathlib
import typing
import yaml


def load_data(input_dir: pathlib.Path) -> typing.Tuple[dict, dict]:
    ext: str = ".tsv"
    sep: str = "\t"
    dtypes: dict = {
        "ssp": {
            "i": numpy.int16,
            "j": numpy.int16,
            "k": numpy.int16,
            "pi": numpy.float32,
            "p": numpy.float32,
        },
        "join_ssp": {
            "dim": numpy.int16,
            "n": numpy.int16,
            "pi": numpy.float32,
            "p": numpy.float32,
        },
        "avg_nt": {
            "dim": numpy.int16,
            "avg": numpy.float32,
            "p": numpy.float32,
        },
    }

    with open(input_dir / "meta.yml", "r") as file:
        cfg = yaml.safe_load(file)

    ds: dict = {}

    # * Walks throught the .tsv files contained in the input directory and load
    # * the data in a pandas.DataFrame.
    for p in input_dir.rglob("*"):
        if p.is_file() and p.suffix == ext:
            fname: str = p.name.replace(ext, "")

            ds[fname] = pandas.read_csv(
                p,
                sep=sep,
                header=0,
                dtype=dtypes[fname],
            )

    return (cfg, ds)


def gen_joined_ssp(df: pandas.DataFrame, dim: str) -> pandas.DataFrame:
    return df.groupby([dim])["p"].sum()


def gen_avg_num_tasks(df: pandas.DataFrame, dim: str) -> numpy.float32:
    joint_ssp_df: pandas.DataFrame = gen_joined_ssp(df, dim)
    return ((joint_ssp_df[dim] + 1) * joint_ssp_df.p).sum()
