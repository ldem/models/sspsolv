# SSPSolv 2.0.0
**Jonathan Heno** <jonathan.heno@pm.me>

Steady state probabilities solver for **continuous-time Markov chains**.
This application is a Rust implementation of the algorithm described in the Seongryong Na article[^1] (2014) that depicts a faster variant of the power method, generally used to process this type of problem.

[^1]: [Na, Seongryong. *Numerical iteration for stationary probabilities of Markov chains.* Communications for Statistical Applications and Methods 21.6 (2014): 513-520.](https://www.koreascience.or.kr/article/JAKO201435648478211.page)

## Requirements
In order to draw the steady state distribution, you should have **Python v3.8** installed on your computer. To generate the appropriate python environment, a requirement file has been added to this project. You just have to run the following command, to load the needed packages from the `pip` command into your current python configuration:
```sh
pip install -r requirements.txt
```

## Example
```
sspsolv -vv -o ./out/na na
sspsolv -v --epsilon 1.0e-5 -o ./out/dqs dqs --file cfg/dqs.yml
sspsolv -v -e 1.0e-5 -o ./out/dqs dqs --lambda 5.0 -p 0.6 --mu.1 14.0 --mu 5.0 --mu.2 10.0 --reward 0.4 --c.1 0.4 --c.2 0.6 --dim.transit 10 --dim.s2 10
```

## Usage
```
    sspsolv [FLAGS] [OPTIONS] --outdir <outdir> [SUBCOMMAND]

FLAGS:
    -c, --clear      Clears the content of the output folder if it already exists
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v               Sets the level of verbosity

OPTIONS:
    -d, --dist.init <dist.init>    Type of initial state probability distribution to give [default: eq]  [possible values: deg, eq]
    -e, --epsilon <epsilon>        Model's precision [default: 1.0e-4]
    -o, --outdir <outdir>          Path to output directory (creates it if it does not exist)


SUBCOMMANDS:
    dqs     Finds the steady state probability vector π of the double queue system defined in the following article: https://bit.ly/3hfDbrw
    help    Prints this message or the help of the given subcommand(s)
    na      Solves the example given in [1].
```

