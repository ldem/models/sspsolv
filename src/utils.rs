use ndarray::parallel::prelude::*;

pub fn to_state(i: usize, dims: &[usize]) -> Vec<usize> {
  let mut rem: usize = i;
  let mut pos: Vec<usize> = vec![];

  if dims.len() > 1 {
    for dim_idx in 1usize..dims.len() {
      let size: usize =
        dims[dim_idx..].iter().fold(1usize, |acc, dim| acc * dim);
      pos.push(rem / size);
      rem %= size;
    }
  }

  pos.push(rem);

  pos
}

pub fn dims_len(dims: &[usize]) -> Vec<usize> {
  dims
    .iter()
    .enumerate()
    .map(|(i, _dim)| {
      if i == dims.len() - 1 {
        1
      } else {
        dims[(i + 1)..]
          .iter()
          .fold(1, |acc: usize, dim: &usize| acc * dim)
      }
    })
    .collect::<Vec<usize>>()
}

pub fn num_states(dims: &[usize]) -> usize {
  dims.iter().fold(1usize, |acc, dim| acc * dim)
}

pub fn gen_indices(dims_info: &[(usize, usize)], index: usize) -> Vec<usize> {
  let mut indices: Vec<usize> = vec![];

  if dims_info.len() == 1 {
    (0..dims_info[0].0)
      .into_iter()
      .for_each(|i| indices.push(index + i * dims_info[0].1));
  } else {
    (0..dims_info[0].0).into_iter().for_each(|i| {
      indices.append(&mut gen_indices(
        &dims_info[1..],
        index + i * dims_info[0].1,
      ))
    })
  }

  indices.sort();
  indices
}

/// Generates the joined steady state probability vector π(i) resulting from the
/// steady state probability vector π(i, j, k, ..).
/// # Parameters
/// - `join_dim`: Couple representing the `i`th dimension
/// characteristics:
///   - Number of states covered by the dimension `i`
///   - Length of the `i`th dimension
/// - `agg_dims`: List of tuples `(state_size, istate_size)` of the other
/// dimensions
/// - `pi`: The steady state probability vector
/// # Returns
/// An array of couples that defines the dimension index, its indexed and the
/// number of states covered by this same dimension (its length):
/// `[ (dim_idx, state_idx, dim_len) ]`
pub fn gen_joined_ssp(
  join_dim: (usize, usize),
  agg_dims: &[(usize, usize)],
  pi: ndarray::ArrayView1<f32>,
) -> Vec<f32> {
  (0..join_dim.0)
    .into_par_iter()
    .map(|i: usize| {
      let indices: &[usize] = &gen_indices(agg_dims, i * join_dim.1);
      indices
        .iter()
        .fold(0.0, |acc: f32, istate: &usize| acc + pi[*istate])
    })
    .collect::<Vec<f32>>()
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_indices_generator() {
    assert_eq!(vec![1, 4, 7, 10, 13, 16], gen_indices(&[(2, 9), (3, 3)], 1));
  }

  #[test]
  fn test_indices_generator_from_single_element_array() {
    assert_eq!(vec![0, 1, 2], gen_indices(&[(3, 1)], 0));
  }
}
