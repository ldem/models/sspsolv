use ndarray::parallel::prelude::*;

#[derive(Debug, Clone, serde_derive::Deserialize)]
pub struct DqsCfg {
  pub lambda: f32,
  pub r: f32,
  pub c_1: f32,
  pub c_2: f32,
  pub mu_1: f32,
  pub mu: f32,
  pub mu_2: f32,
  pub dim_transit: usize,
  pub dim_s2: usize,
}

impl DqsCfg {
  pub fn to_meta(
    &self,
  ) -> std::collections::HashMap<String, serde_yaml::Value> {
    let mut meta: std::collections::HashMap<String, serde_yaml::Value> =
      std::collections::HashMap::<String, serde_yaml::Value>::new();

    meta.insert(
      "lambda".to_string(),
      serde_yaml::to_value(self.lambda).unwrap(),
    );

    meta.insert("r".to_string(), serde_yaml::to_value(self.r).unwrap());
    meta.insert("c.1".to_string(), serde_yaml::to_value(self.c_1).unwrap());
    meta.insert("c.2".to_string(), serde_yaml::to_value(self.c_2).unwrap());

    meta.insert("mu.1".to_string(), serde_yaml::to_value(self.mu_1).unwrap());
    meta.insert("mu".to_string(), serde_yaml::to_value(self.mu).unwrap());
    meta.insert("mu.2".to_string(), serde_yaml::to_value(self.mu_2).unwrap());

    meta
  }
}

pub fn build_transition_rate_matrix(
  dims: &[usize; 3],
  lambda_1: f32,
  lambda_2: f32,
  mu_1: f32,
  mu: f32,
  mu_2: f32,
) -> ndarray::Array2<f32> {
  let num_states: usize = crate::utils::num_states(dims);
  let dims_len: Vec<usize> = crate::utils::dims_len(dims);

  let mut q: ndarray::Array2<f32> =
    ndarray::Array2::<f32>::zeros([num_states; 2]);

  q.outer_iter_mut()
    .into_par_iter()
    .enumerate()
    .for_each(|(i, mut row)| {
      let state: &[usize] = &crate::utils::to_state(i, dims);

      // * Before diagonal
      if i > 0 && state[2] > 0 {
        // * If there is at least one task to perform in S2, we can pass from
        // * state (i, j, k) to (i, j, k-1).
        row[i - 1] = mu_2;
      }

      if state[1] > 0 && state[2] < dims[2] - 1 {
        // * If a task is in transit and S2 is not saturated we can append a new
        // * process to this latter, going from (i, j, k) to (i, j-1, k+1).
        row[i - dims_len[1] + 1] = (state[1] as f32) * mu;
      }

      if state[0] > 0 {
        row[i - dims_len[0]] = mu_1;
      }

      // * After diagonal
      if i + dims_len[1] < num_states && state[1] < dims[1] - 1 {
        row[i + dims_len[1]] = lambda_2;
      }
      if i + dims_len[0] < num_states {
        row[i + dims_len[0]] = lambda_1;
      }
    });

  q
}

pub fn init(
  lambda: f32,
  p: f32,
  mu_1: f32,
  mu: f32,
  mu_2: f32,
  r: f32,
  c_1: f32,
  _c_2: f32,
  dim_transit: usize,
  dim_s2: usize,
) -> (ndarray::Array2<f32>, ndarray::Array2<f32>, Vec<usize>) {
  let dims: [usize; 3] =
    [(r * mu_1 / c_1).floor() as usize, dim_transit, dim_s2];
  let q: ndarray::Array2<f32> = build_transition_rate_matrix(
    &dims,
    lambda * p,
    lambda * (1. - p),
    mu_1,
    mu,
    mu_2,
  );
  let p: ndarray::Array2<f32> = crate::dist_from(&q);

  (q, p, dims.to_vec())
}

// ! Unthreaded version
// fn build_transition_rate_matrix(
//     dims  : &[usize;3],
//     lambda: &[f32;2],
//     mu    : &[f32;3]
// ) -> ndarray::Array2<f32> {
//     let size_from_dims = |dims: &[usize]| -> usize {
//         dims.iter().fold(1usize, |acc, dim| acc * dim)
//     };

//     let num_states: usize = size_from_dims(dims);
//     let dim_i: usize = size_from_dims(&dims[1..]);
//     let dim_j: usize = dims[2];

//     let mut q: ndarray::Array2<f32> = ndarray::Array2::<f32>::zeros([num_states;2]);

//     let update = |q: &mut ndarray::Array2<f32>, pos: &[usize;2], p: f32| {
//         q[*pos] = p;
//         q[[pos[0], pos[0]]] -= p;
//     };

//     // * Generator initialization
//     for i in 0..num_states {
//         // * Before diagonal
//         if i > 0 {
//             update(&mut q, &[i, i - 1], mu[2]);
//         }
//         if i >= dim_j {
//             update(&mut q, &[i, i - dim_j], mu[1] * (i / dim_j) as f32);
//         }
//         if i >= dim_i {
//             update(&mut q, &[i, i - dim_i], mu[0]);
//         }

//         // * After diagonal
//         if i + dim_j < num_states {
//             update(&mut q, &[i, i + dim_j], lambda[1]);
//         }
//         if i + dim_i < num_states {
//             update(&mut q, &[i, i + dim_i], lambda[0]);
//         }
//     }

//     q
// }
