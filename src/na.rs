pub fn build_transition_rate_matrix() -> ndarray::Array2<f32> {
  ndarray::arr2(&[
    [0., 1., 0., 0., 0.],
    [2., 0., 3., 0., 0.],
    [0., 4., 0., 4., 0.],
    [0., 0., 2., 0., 3.],
    [0., 0., 0., 1., 0.],
  ])
}

pub fn init() -> (ndarray::Array2<f32>, ndarray::Array2<f32>, Vec<usize>) {
  let q: ndarray::Array2<f32> = build_transition_rate_matrix();
  let p: ndarray::Array2<f32> = crate::dist_from(&q);

  let dims: Vec<usize> = vec![*(&q.shape()[0])];

  (q, p, dims)
}
