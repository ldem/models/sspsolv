use std::io::Write;

use ndarray::parallel::prelude::*;

mod dqs;
mod na;
mod utils;

const LOG_COUNTER: usize = 10;

fn build_equal_init_dist(num_states: usize) -> ndarray::Array1<f32> {
  ndarray::Array1::<f32>::ones(num_states) / (num_states as f32)
}

fn build_degenerate_init_dist(num_states: usize) -> ndarray::Array1<f32> {
  let mut pi: ndarray::Array1<f32> = ndarray::Array1::<f32>::zeros(num_states);
  pi[0] = 1.0;
  pi
}

fn dist_from(q: &ndarray::Array2<f32>) -> ndarray::Array2<f32> {
  let mut p: ndarray::Array2<f32> = q.clone();
  let rates: ndarray::Array1<f32> = state_transitions_from(q.view());

  for (i, &rate) in rates.iter().enumerate() {
    let mut row = p.row_mut(i);
    row /= rate;
  }

  p
}

fn state_transitions_from(q: ndarray::ArrayView2<f32>) -> ndarray::Array1<f32> {
  ndarray::arr1(
    &q.outer_iter()
      .into_par_iter()
      .map(|row| row.sum())
      .collect::<Vec<f32>>(),
  )
}

fn update(
  p: ndarray::ArrayView2<f32>,
  pi: ndarray::ArrayView1<f32>,
  pi_star: ndarray::ArrayView1<f32>,
) -> (ndarray::Array1<f32>, ndarray::Array1<f32>, f32) {
  let next_pi: ndarray::Array1<f32> = ndarray::arr1(
    &p.axis_iter(ndarray::Axis(1))
      .into_par_iter()
      .map(|col| pi.dot(&col))
      .collect::<Vec<f32>>(),
  );

  // next_pi = normalize(next_pi.view());

  let next_pi_star =
    ndarray::Zip::from(next_pi.view()).and(pi).par_map_collect(
      |new_value: &f32, old_value: &f32| (new_value + old_value) / 2.0,
    );

  let abs_max_diff: f32 = (&next_pi_star - &pi_star)
    .into_par_iter()
    .max_by(|&a, &b| a.abs().partial_cmp(&b.abs()).unwrap())
    .unwrap()
    .abs();

  (next_pi, next_pi_star, abs_max_diff)
}

fn to_continuous(
  pi: ndarray::ArrayView1<f32>,
  rates: ndarray::ArrayView1<f32>,
) -> ndarray::Array1<f32> {
  let rates: ndarray::Array1<f32> = 1.0 / &rates;
  let sum: f32 = pi.dot(&rates);

  (&pi * &rates) / sum
}

fn save_meta(
  out: &std::path::PathBuf,
  meta: &std::collections::BTreeMap<String, serde_yaml::Value>,
) -> Result<(), Box<dyn std::error::Error>> {
  match serde_yaml::to_writer(
    std::fs::File::create(&out.join("meta.yml"))?,
    meta,
  ) {
    Ok(()) => Ok(()),
    Err(msg) => Err(msg.into()),
  }
}

fn save_ssp(
  out: &std::path::PathBuf,
  dims: &[usize],
  p: Option<&serde_yaml::Value>,
  pi: ndarray::ArrayView1<f32>,
) -> Result<(), Box<dyn std::error::Error>> {
  let path: std::path::PathBuf = out.join("ssp.tsv");

  let mut file: std::fs::File = if path.exists() && path.is_file() {
    std::fs::OpenOptions::new()
      .write(true)
      .append(true)
      .open(path)?
  } else {
    let mut file: std::fs::File = std::fs::File::create(path)?;
    file.write_all(
      format!("i\tj\tk\tpi{}\n", if p.is_some() { "\tp" } else { "" })
        .as_bytes(),
    )?;
    file
  };

  pi.iter().enumerate().for_each(|(i, value)| {
    let pos: Vec<usize> = utils::to_state(i, dims);
    let _ = file.write_all(
      format!(
        "{}\t{}\t{}\t{:.5}{}\n",
        pos[0],
        pos[1],
        pos[2],
        value,
        if p.is_some() {
          format!("\t{:.5}", p.unwrap().as_f64().unwrap())
        } else {
          "".to_string()
        }
      )
      .as_bytes(),
    );
  });

  Ok(())
}

fn save_joined_ssp(
  out: &std::path::PathBuf,
  dims: &[usize],
  p: f32,
  pi_star: ndarray::ArrayView1<f32>,
) -> Result<(), Box<dyn std::error::Error>> {
  let path: std::path::PathBuf = out.join("join_ssp.tsv");

  let mut file: std::fs::File = if path.exists() && path.is_file() {
    std::fs::OpenOptions::new()
      .write(true)
      .append(true)
      .open(path)?
  } else {
    let mut file: std::fs::File = std::fs::File::create(path)?;
    file.write_all(b"dim\tn\tpi\tp\n")?;
    file
  };

  let dims_len: &[usize] = &utils::dims_len(dims);

  for (dim_idx, (&size, &len)) in dims.iter().zip(dims_len).enumerate() {
    let agg_dims: &[(usize, usize)] = &dims
      .iter()
      .zip(dims_len)
      .enumerate()
      .filter(|(idx, _info)| *idx != dim_idx)
      .map(|(_idx, (&size, &len))| (size, len))
      .collect::<Vec<_>>();

    let pi_joined: &[f32] =
      &utils::gen_joined_ssp((size, len), agg_dims, pi_star);

    pi_joined.iter().enumerate().for_each(|(i, p_joined)| {
      let _ = file.write_all(
        format!("{}\t{}\t{:.5}\t{:.5}\n", dim_idx, i, p_joined, p).as_bytes(),
      );
    });
  }

  Ok(())
}

fn save_avg_num_tasks(
  out: &std::path::PathBuf,
  dims: &[usize],
  p: f32,
  pi_star: ndarray::ArrayView1<f32>,
) -> Result<(), Box<dyn std::error::Error>> {
  let path: std::path::PathBuf = out.join("avg_nt.tsv");

  let mut file: std::fs::File = if path.exists() && path.is_file() {
    std::fs::OpenOptions::new()
      .write(true)
      .append(true)
      .open(path)?
  } else {
    let mut file: std::fs::File = std::fs::File::create(path)?;
    file.write_all(b"dim\tavg\tp\n")?;
    file
  };

  let dims_len: &[usize] = &utils::dims_len(dims);

  for (dim_idx, (&size, &len)) in dims.iter().zip(dims_len).enumerate() {
    let agg_dims: &[(usize, usize)] = &dims
      .iter()
      .zip(dims_len)
      .enumerate()
      .filter(|(idx, _info)| *idx != dim_idx)
      .map(|(_idx, (&size, &len))| (size, len))
      .collect::<Vec<_>>();

    let avg_num_tasks: f32 =
      utils::gen_joined_ssp((size, len), agg_dims, pi_star)
        .iter()
        .enumerate()
        .fold(0.0, |acc: f32, (i, p_join)| acc + (i + 1) as f32 * p_join);

    let _ = file.write_all(
      format!("{}\t{:.5}\t{:.5}\n", dim_idx, avg_num_tasks, p).as_bytes(),
    );
  }

  Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
  // * Loading user input parser
  let yaml = clap::load_yaml!("../cli.yml");
  let matcher = clap::App::from_yaml(yaml).get_matches();
  let mut meta: std::collections::BTreeMap<String, serde_yaml::Value> =
    std::collections::BTreeMap::<String, serde_yaml::Value>::new();

  // * Initialization
  let verbosity: u64 = matcher.occurrences_of("verbosity");

  let epsilon: f32 = matcher.value_of("epsilon").unwrap().parse::<f32>()?;

  meta.insert("epsilon".to_string(), serde_yaml::to_value(epsilon)?);

  // * Generates the output folder
  let out: std::path::PathBuf =
    std::path::PathBuf::from(matcher.value_of("outdir").unwrap());

  if out.exists() {
    if out.is_dir() {
      if matcher.is_present("clear") {
        std::fs::remove_dir_all(&out)?;
        std::fs::create_dir_all(&out)?;
      }
    } else {
      panic!("{:?} is not a directory..", &out);
    }
  } else {
    std::fs::create_dir_all(&out)?;
  }

  let (mut q, p, dims) = if matcher.subcommand_matches("na").is_some() {
    meta.insert("app".to_string(), serde_yaml::to_value("na")?);
    na::init()
  } else {
    meta.insert("app".to_string(), serde_yaml::to_value("dqs")?);

    let m = matcher.subcommand_matches("dqs").unwrap();

    let p: f32 = clap::value_t!(m.value_of("p"), f32)?;
    meta.insert("p".to_string(), serde_yaml::to_value(p)?);

    let cfg: dqs::DqsCfg = if m.is_present("file") {
      let path: std::path::PathBuf =
        std::path::PathBuf::from(m.value_of("file").unwrap());
      let file: std::fs::File = std::fs::File::open(path)?;
      let reader = std::io::BufReader::new(file);

      serde_yaml::from_reader(reader)?
    } else {
      dqs::DqsCfg {
        lambda: clap::value_t!(m.value_of("lambda"), f32)?,
        r: clap::value_t!(m.value_of("reward"), f32)?,
        c_1: clap::value_t!(m.value_of("c.1"), f32)?,
        c_2: clap::value_t!(m.value_of("c.2"), f32)?,
        mu_1: clap::value_t!(m.value_of("mu.1"), f32)?,
        mu: clap::value_t!(m.value_of("mu"), f32)?,
        mu_2: clap::value_t!(m.value_of("mu.2"), f32)?,
        dim_transit: clap::value_t!(m.value_of("dim.transit"), usize)?,
        dim_s2: clap::value_t!(m.value_of("dim.s2"), usize)?,
      }
    };

    // * Updating metadata with the retrieved configuration
    for (key, value) in cfg.to_meta() {
      meta.insert(key, value);
    }

    dqs::init(
      cfg.lambda,
      p,
      cfg.mu_1,
      cfg.mu,
      cfg.mu_2,
      cfg.r,
      cfg.c_1,
      cfg.c_2,
      cfg.dim_transit,
      cfg.dim_s2,
    )
  };

  meta.insert("dims".to_string(), serde_yaml::to_value(&dims)?);

  let state_transitions: ndarray::Array1<f32> =
    state_transitions_from(q.view());

  let mut pi: ndarray::Array1<f32> =
    if &matcher.value_of("dist.init").unwrap().to_lowercase() == "eq" {
      meta.insert("dist".to_string(), serde_yaml::to_value("eq")?);
      build_equal_init_dist(q.shape()[0])
    } else {
      meta.insert("dist".to_string(), serde_yaml::to_value("deg")?);
      build_degenerate_init_dist(q.shape()[0])
    };

  let mut pi_star: ndarray::Array1<f32> =
    ndarray::arr1(&vec![0.; q.shape()[0]]);

  if verbosity > 1 {
    println!("𝓓\n{:?}\n---", &dims);
    println!("𝓠'\n{:.2?}\n---", q.view());
    println!("λ\n{:.5?}\n---", state_transitions.view());
    println!("𝓟\n{:.5?}\n---", p.view());
    println!("π@0\n{:.5?}\n---", pi.view());
  }

  let mut abs_max_diff: f32 = f32::INFINITY;
  let mut t: usize = 0;

  println!("Processing recurrence...");
  while abs_max_diff > epsilon {
    let (next_pi, next_pi_star, diff) =
      update(p.view(), pi.view(), pi_star.view());

    if verbosity >= 1 && t % LOG_COUNTER == 0 {
      println!("@{}", t);
      println!("  π   : {:.5?}", pi.view());
      println!("  π*  : {:.5?}", pi_star.view());
      println!("  diff: {:.7}", diff);
      println!("  ---");
    }

    abs_max_diff = diff;
    pi = next_pi;
    pi_star = next_pi_star;
    t += 1;
  }

  meta.insert("it".to_string(), serde_yaml::to_value(t - 1)?);
  meta.insert("diff".to_string(), serde_yaml::to_value(abs_max_diff)?);

  let pi_star = to_continuous(pi_star.view(), state_transitions.view());
  if verbosity >= 1 {
    println!("π* continuous: {:.5?}", pi_star.view());
  }

  let (&idx, &max) =
    ndarray::Zip::from(&ndarray::Array::range(0., q.shape()[0] as f32, 1.))
      .and(&pi_star)
      .into_par_iter()
      .max_by(|&a, &b| a.1.partial_cmp(b.1).unwrap())
      .unwrap();

  meta.insert(
    "max.state".to_string(),
    serde_yaml::to_value(utils::to_state(idx as usize, &dims))?,
  );
  meta.insert("max.p".to_string(), serde_yaml::to_value(max)?);

  if verbosity >= 1 {
    println!(
      "Max π*{:?}@{}: {:.7}%\n---",
      utils::to_state(idx as usize, &dims),
      t - 1,
      max * 100.0,
    );
  }

  if verbosity > 1 {
    // * Transforms q as a true continuous time markov chain generator (i.e. a
    // * matrix where the sum of the elements of each row is null)
    q.axis_iter_mut(ndarray::Axis(0))
      .into_par_iter()
      .enumerate()
      .for_each(|(i, mut row)| {
        row[i] = -state_transitions.view()[i];
      });

    // ? Sum π = 1.0
    println!("Sum π*: {:.7}", &pi.sum());

    // ? π*𝓠 = 0 (continuous time)
    println!("π*𝓠   : {:.5?}", &pi_star.dot(&q));

    // ? π𝓟 = π (discrete time)
    println!("π     : {:.5?}", pi.view());
    println!("π𝓟    : {:.5?}", &pi.dot(&p));
  }

  // * Saves π* and configuration to the output folder
  save_ssp(&out, &dims, meta.get("p"), pi_star.view())?;

  if dims.len() >= 1 && meta.get("p").is_some() {
    let p: f32 = meta.get("p").unwrap().as_f64().unwrap() as f32;

    save_joined_ssp(&out, &dims, p, pi_star.view())?;
    save_avg_num_tasks(&out, &dims, p, pi_star.view())?;
  }

  let _ = meta.remove("p");
  save_meta(&out, &meta)?;

  Ok(())
}
